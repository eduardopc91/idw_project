const express = require('express');
const bodyParser = require('body-parser')
const app = express();

const ObjectId = require('mongodb').ObjectID
const MongoClient = require('mongodb').MongoClient
const uri = "mongodb+srv://admin:admin@cluster0-b6rvl.mongodb.net/test?retryWrites=true";

MongoClient.connect(uri, (err, client) => {
    if (err) return console.log(err)
    db = client.db('students')
  
    app.listen(3000, () => {
      console.log('Server running on port 3000')
    })
})

app.use(bodyParser.urlencoded({ extended: true }))

app.set('view engine', 'ejs')

app.get('/', (req, res) => {
    res.render('index.ejs',{
        message : 'Bem Vindos!'
    })
})

// app.get('/', (req, res) => {
//     var cursor = db.collection('name').find()
// })

app.get('/register', (req, res) => {
    db.collection('name').find().toArray((err, results) => {
        if (err) return console.log(err)
        res.render('show.ejs', { data: results })

    })
})

app.post('/register', (req, res) => {
    db.collection('name').insertOne(req.body, (err, result) => {
      if (err) return console.log(err)
  
      console.log('Salvo no Banco de Dados')
      res.redirect('/register')
    })
})

app.get('/edit/:id', (req, res) => {
  var id = req.params.id

  db.collection('name').find(ObjectId(id)).toArray((err, result) => {
    if (err) return res.send(err)
    res.render('edit.ejs', { data: result })
  })
})

app.post('/edit/:id', (req, res) => {
  var id = req.params.id
  var name = req.body.name
  var surname = req.body.surname

  db.collection('name').updateOne({_id: ObjectId(id)}, {
    $set: {
      name: name,
      surname: surname
    }
  }, (err, result) => {
    if (err) return res.send(err)
    res.redirect('/register')
    console.log('Atualizado no Banco de Dados')
  })
})

app.get('/delete/:id', (req, res) => {
  var id = req.params.id

  db.collection('name').deleteOne({_id: ObjectId(id)}, (err, result) => {
    if (err) return res.send(500, err)
    console.log('Deletado do Banco de Dados!')
    res.redirect('/register')
  })
})

module.exports = app